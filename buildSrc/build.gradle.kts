plugins {
    `kotlin-dsl`
}

repositories {
    jcenter()
}

dependencies {
    compile("commons-io:commons-io:2.6")
    compile("com.fasterxml.jackson.core:jackson-core:2.9.2")
    compile("com.fasterxml.jackson.core:jackson-databind:2.9.2")
    compile("com.fasterxml.jackson.core:jackson-annotations:2.9.2")
    compile("com.fasterxml.jackson.module:jackson-module-parameter-names:2.9.2")
    compile("com.fasterxml.jackson.datatype:jackson-datatype-jdk8:2.9.2")
    compile("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.9.2")
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.2")
    compile("org.apache.httpcomponents:httpclient:4.5.3")
    compile("com.github.zafarkhaja:java-semver:0.9.0")
}
