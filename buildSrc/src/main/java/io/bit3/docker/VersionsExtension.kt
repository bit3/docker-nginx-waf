package io.bit3.docker

import org.gradle.api.Project
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider

open class VersionsExtension(
        project: Project,
        nginxVersionProvider: Provider<DockerHubClient.Tag>,
        modSecurityVersionProvider: Provider<GitHubClient.Release>,
        modSecurityNginxVersionProvider: Provider<GitHubClient.Release>,
        owaspVersionProvider: Provider<GitHubClient.Release>,
        imageVersionProvider: Provider<String>,
        imageVersionsProvider: Provider<List<String>>
) {
    val nginxVersion: Property<DockerHubClient.Tag> = project.objects.property(DockerHubClient.Tag::class.java)
    val modSecurityVersion: Property<GitHubClient.Release> = project.objects.property(GitHubClient.Release::class.java)
    val modSecurityNginxVersion: Property<GitHubClient.Release> = project.objects.property(GitHubClient.Release::class.java)
    val owaspVersion: Property<GitHubClient.Release> = project.objects.property(GitHubClient.Release::class.java)
    val imageVersion: Property<String> = project.objects.property(String::class.java)
    val imageVersions: ListProperty<String> = project.objects.listProperty(String::class.java)

    init {
        this.nginxVersion.set(nginxVersionProvider)
        this.modSecurityVersion.set(modSecurityVersionProvider)
        this.modSecurityNginxVersion.set(modSecurityNginxVersionProvider)
        this.owaspVersion.set(owaspVersionProvider)
        this.imageVersion.set(imageVersionProvider)
        this.imageVersions.set(imageVersionsProvider)
    }
}
