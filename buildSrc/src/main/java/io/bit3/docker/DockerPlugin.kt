package io.bit3.docker

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule
import org.gradle.api.Plugin
import org.gradle.api.Project
import java.io.File

class DockerPlugin : Plugin<Project> {

    override fun apply(project: Project) {
        val objectMapper = ObjectMapper()
                .registerModule(ParameterNamesModule())
                .registerModule(Jdk8Module())
                .registerModule(JavaTimeModule())
                .registerModule(KotlinModule())

        val cacheDir = File(project.buildDir, "versions")
        cacheDir.mkdirs()

        val dockerHubClient = DockerHubClient(objectMapper)
        val gitHubClient = GitHubClient(project, objectMapper)

        val nginxVersionProvider = project.provider {
            if (project.hasProperty("NGINX_VERSION")) {
                DockerHubClient.Tag(project.findProject("NGINX_VERSION").toString())
            } else {
                val file = File(cacheDir, "nginxVersion.json")

                if (file.exists()) {
                    objectMapper.readValue(file, DockerHubClient.Tag::class.java)
                } else {
                    val nginxVersions = dockerHubClient.listTags("nginx")
                    val nginxVersion = nginxVersions.max()
                    objectMapper.writeValue(file, nginxVersion)
                    nginxVersion
                }
            }
        }

        val modSecurityVersionProvider = project.provider {
            if (project.hasProperty("MOD_SECURITY_VERSION")) {
                GitHubClient.Release(project.findProject("MOD_SECURITY_VERSION").toString())
            } else {
                val file = File(cacheDir, "modSecurityVersion.json")

                if (file.exists()) {
                    objectMapper.readValue(file, GitHubClient.Release::class.java)
                } else {
                    val modSecurityVersions = gitHubClient.listReleases("SpiderLabs/ModSecurity")
                    val modSecurityVersion = modSecurityVersions.max()
                    objectMapper.writeValue(file, modSecurityVersion)
                    modSecurityVersion
                }
            }
        }

        val modSecurityNginxVersionProvider = project.provider {
            if (project.hasProperty("MOD_SECURITY_NGINX_VERSION")) {
                GitHubClient.Release(project.findProject("MOD_SECURITY_NGINX_VERSION").toString())
            } else {
                val file = File(cacheDir, "modSecurityNginxVersion.json")

                if (file.exists()) {
                    objectMapper.readValue(file, GitHubClient.Release::class.java)
                } else {
                    val modSecurityNginxVersions = gitHubClient.listReleases("SpiderLabs/ModSecurity-nginx")
                    val modSecurityNginxVersion = modSecurityNginxVersions.max()
                    objectMapper.writeValue(file, modSecurityNginxVersion)
                    modSecurityNginxVersion
                }
            }
        }

        val owaspVersionProvider = project.provider {
            if (project.hasProperty("OWASP_VERSION")) {
                GitHubClient.Release(project.findProject("OWASP_VERSION").toString())
            } else {
                val file = File(cacheDir, "owaspVersion.json")

                if (file.exists()) {
                    objectMapper.readValue(file, GitHubClient.Release::class.java)
                } else {
                    val owaspVersions = gitHubClient.listReleases("SpiderLabs/owasp-modsecurity-crs")
                    val owaspVersion = owaspVersions.max()
                    objectMapper.writeValue(file, owaspVersion)
                    owaspVersion
                }
            }
        }

        val imageVersionProvider = project.provider {
            val nginxVersionTag = nginxVersionProvider.get()
            val modSecurityVersionRelease = modSecurityVersionProvider.get()
            val owaspVersionRelease = owaspVersionProvider.get()

            val modSecurityVersion = modSecurityVersionRelease.version ?: modSecurityVersionRelease.name
            val nginxVersion = nginxVersionTag.version ?: nginxVersionTag.name
            val owaspVersion = owaspVersionRelease.version ?: owaspVersionRelease.name

            "$nginxVersion-modsecurity$modSecurityVersion-owasp$owaspVersion"
        }

        val imageVersionsProvider = project.provider {
            val nginxVersionTag = nginxVersionProvider.get()
            val modSecurityVersionRelease = modSecurityVersionProvider.get()
            val owaspVersionRelease = owaspVersionProvider.get()

            val nginxVersions = mutableListOf<String>()
            val modSecurityVersions = mutableListOf<String>()
            val owaspVersions = mutableListOf<String>()

            if (null == nginxVersionTag.version) {
                nginxVersions.add(nginxVersionTag.name)
            } else {
                val nginxVersion = nginxVersionTag.version
                nginxVersions.add("$nginxVersion")
                nginxVersions.add("${nginxVersion.majorVersion}.${nginxVersion.minorVersion}")
                nginxVersions.add("${nginxVersion.majorVersion}")
            }

            if (null == modSecurityVersionRelease.version) {
                modSecurityVersions.add(modSecurityVersionRelease.name)
            } else {
                val modSecurityVersion = modSecurityVersionRelease.version
                modSecurityVersions.add("$modSecurityVersion")
                modSecurityVersions.add("${modSecurityVersion.majorVersion}.${modSecurityVersion.minorVersion}")
                modSecurityVersions.add("${modSecurityVersion.majorVersion}")
            }

            if (null == owaspVersionRelease.version) {
                owaspVersions.add(owaspVersionRelease.name)
            } else {
                val owaspVersion = owaspVersionRelease.version
                owaspVersions.add("$owaspVersion")
                owaspVersions.add("${owaspVersion.majorVersion}.${owaspVersion.minorVersion}")
                owaspVersions.add("${owaspVersion.majorVersion}")
            }

            val list = mutableListOf<String>()

            nginxVersions.forEach { nginxVersion ->
                list.add(nginxVersion)

                owaspVersions.forEach { owaspVersion ->
                    list.add("$nginxVersion-owasp$owaspVersion")
                    modSecurityVersions.forEach { modSecurityVersion ->
                        list.add("$nginxVersion-modsecurity$modSecurityVersion-owasp$owaspVersion")
                    }
                }
            }

            if (null != nginxVersionTag.version && null != modSecurityVersionRelease.version && null != owaspVersionRelease.version) {
                list.add("latest")
            }

            list
        }

        project.extensions.create(
                "versions",
                VersionsExtension::class.java,
                project,
                nginxVersionProvider,
                modSecurityVersionProvider,
                modSecurityNginxVersionProvider,
                owaspVersionProvider,
                imageVersionProvider,
                imageVersionsProvider
        )
    }

}
