package io.bit3.docker

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule
import com.github.zafarkhaja.semver.Version
import org.apache.commons.io.IOUtils
import java.net.URL
import java.nio.charset.StandardCharsets
import java.util.*

class DockerHubClient(private val objectMapper: ObjectMapper) {
    private val tagListType: TypeReference<LinkedList<Tag>> = object : TypeReference<LinkedList<Tag>>() {
    }

    fun listTags(id: String): List<Tag> {
        val url = URL("https://registry.hub.docker.com/v1/repositories/$id/tags")
        return url.openStream().use { inputStream ->
            val json = IOUtils.toString(inputStream, StandardCharsets.UTF_8)
            val tags: List<Tag> = objectMapper.readValue(json, tagListType)
            tags.filter { null != it.version }
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Tag(val name: String) : Comparable<Tag> {
        @JsonIgnore
        val version = if (name.matches(Regex("\\d+\\.\\d+\\.\\d+"))) Version.valueOf(name)
        else null

        val versionOrName: String get() {
            return version?.toString() ?: name
        }

        override fun compareTo(other: Tag): Int {
            return version!!.compareTo(other.version!!)
        }
    }
}
