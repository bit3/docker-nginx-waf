package io.bit3.docker

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule
import com.github.zafarkhaja.semver.Version
import org.apache.commons.io.IOUtils
import org.gradle.api.Project
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.StandardCharsets
import java.util.*

class GitHubClient(private val project: Project, private val objectMapper: ObjectMapper) {
    private val tagListType: TypeReference<LinkedList<Release>> = object : TypeReference<LinkedList<Release>>() {
    }

    fun listReleases(id: String): List<Release> {
        val url = URL("https://api.github.com/repos/$id/releases")
        val connection = url.openConnection() as HttpURLConnection

        val accessToken = project.findProperty("GITHUB_ACCESS_TOKEN")
        accessToken.let {
            connection.setRequestProperty("Authorization", "token $it")
        }

        return connection.inputStream.use { inputStream ->
            val json = IOUtils.toString(inputStream, StandardCharsets.UTF_8)
            val releases: List<Release> = objectMapper.readValue(json, tagListType)
            releases.filter { null != it.version }
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Release(
            @JsonProperty("tag_name")
            var name: String
    ) : Comparable<Release> {
        @JsonIgnore
        val version = if (name.matches(Regex("v?\\d+\\.\\d+\\.\\d+"))) Version.valueOf(name.replace(Regex("^v"), ""))
        else null

        val versionOrName: String get() {
            return version?.toString() ?: name
        }

        override fun compareTo(other: Release): Int {
            return version!!.compareTo(other.version!!)
        }
    }
}
