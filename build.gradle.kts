import com.bmuschko.gradle.docker.tasks.image.Dockerfile
import com.bmuschko.gradle.docker.tasks.image.DockerBuildImage
import com.bmuschko.gradle.docker.tasks.image.DockerPushImage
import com.bmuschko.gradle.docker.tasks.image.DockerTagImage
import org.apache.tools.ant.filters.ReplaceTokens
import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask

plugins {
    `kotlin-dsl`
    id("io.bit3.docker")
    id("com.github.ben-manes.versions") version "0.20.0"
    id("com.bmuschko.docker-remote-api") version "3.6.2"
}

repositories {
    jcenter()
}

docker {
    if (project.hasProperty("CI_DOCKER_HOST")) {
        url = "${project.findProperty("CI_DOCKER_HOST")}"
        logger.warn("Using docker service ${url}")
    }

    registryCredentials {
        if (project.hasProperty("CI_REGISTRY")) {
            url = project.findProperty("CI_REGISTRY").toString()
        }

        if (project.hasProperty("CI_REGISTRY_USER") && project.hasProperty("CI_REGISTRY_PASSWORD")) {
            username = project.findProperty("CI_REGISTRY_USER").toString()
            password = project.findProperty("CI_REGISTRY_PASSWORD").toString()

            logger.warn("Using docker registry ${username}@${url}")
        }
    }
}

val createDockerfile by tasks.creating(Copy::class) {
    group = "Docker"

    from(".")
    include("Dockerfile.tpl")
    into("build/docker")
    rename("Dockerfile.tpl", "Dockerfile")
    filter(ReplaceTokens::class, mapOf("tokens" to mapOf(
            "NGINX_VERSION" to versions.nginxVersion.get().version.toString(),
            "MOD_SECURITY_VERSION" to versions.modSecurityVersion.get().versionOrName,
            "MOD_SECURITY_NGINX_VERSION" to versions.modSecurityNginxVersion.get().versionOrName,
            "OWASP_CRS_VERSION" to versions.owaspVersion.get().versionOrName
    )))
}

val buildImage by tasks.creating(DockerBuildImage::class) {
    group = "Docker"
    dependsOn(createDockerfile)

    inputDir = file("build/docker")
    tags = versions.imageVersions.get().map {
        "bit3/nginx-waf:$it"
    }.toSet()
}

val pushImageTags = versions.imageVersions.get().map { imageVersion ->
    tasks.create("pushImage${imageVersion.capitalize()}", DockerPushImage::class) {
        group = ""
        dependsOn(buildImage)

        imageName = "bit3/nginx-waf"
        tag = imageVersion
    }
}

val pushImage by tasks.creating {
    group = "Docker"
    dependsOn(pushImageTags)
}

tasks {
    "dependencyUpdates"(DependencyUpdatesTask::class) {
        resolutionStrategy {
            componentSelection {
                all {
                    val rejected = listOf("alpha", "beta", "rc", "cr", "m", "preview")
                            .map { qualifier -> Regex("(?i).*[.-]$qualifier[.\\d-]*") }
                            .any { it.matches(candidate.version) }
                    if (rejected) {
                        reject("Release candidate")
                    }
                }
            }
        }
    }
}
